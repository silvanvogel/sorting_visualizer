# -------------------------------------------------------------------------------------------------------------------
# Algorithm:    Selection Sort
#
# Description:  The selection sort algorithm sorts an array 
#               by repeatedly finding the minimum element (considering ascending order) from unsorted part 
#               and putting it at the beginning. The algorithm maintains two subarrays in a given array.
#
#               The subarray which is already sorted. 
#               Remaining subarray which is unsorted.
#               In every iteration of selection sort, the minimum element (considering ascending order) 
#               from the unsorted subarray is picked and moved to the sorted subarray. 
# ------------------------------------------------------------------------------------------------------------------- 

import time
import colors

def selection_sort(data, drawData, timeTick):
    for i in range(len(data)-1):
        minimum = i
        for k in range(i+1, len(data)):
            if data[k] < data[minimum]:
                minimum = k

        data[minimum], data[i] = data[i], data[minimum]
        drawData(data, [colors.YELLOW if x == minimum or x == i else colors.BLUE for x in range(len(data))] )
        time.sleep(timeTick)
        
    drawData(data, [colors.BLUE for x in range(len(data))])