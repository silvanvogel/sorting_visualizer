# -------------------------------------------------------------------------------------------------------------------
# Algorithm:    Quick Sort
#
# Description:  Like Merge Sort, QuickSort is a Divide and Conquer algorithm. 
#               It picks an element as pivot and partitions the given array around the picked pivot. 
#               There are many different versions of quickSort that pick pivot in different ways. 
#
#               1. Always pick first element as pivot.
#               2. Always pick last element as pivot (implemented below)
#               3. Pick a random element as pivot.
#               4. Pick median as pivot.
#
#               The key process in quick_sort is partition(). 
#               Target of partitions is, given an array and an element x of array as pivot, 
#               put x at its correct position in sorted array and put all smaller elements (smaller than x) before x, 
#               and put all greater elements (greater than x) after x. All this should be done in linear time.
# -------------------------------------------------------------------------------------------------------------------

import time
import colors

def partition(data, start, end):
    i = start + 1
    pivot = data[start]

    for j in range(start+1, end+1):
        if data[j] < pivot:
            data[i], data[j] = data[j], data[i]
            i+=1
    data[start], data[i-1] = data[i-1], data[start]
    return i-1

def quick_sort(data, start, end, drawData, timeTick):
    if start < end:
        pivot_position = partition(data, start, end)
        quick_sort(data, start, pivot_position-1, drawData, timeTick)
        quick_sort(data, pivot_position+1, end, drawData, timeTick)

        drawData(data, [colors.PURPLE if x >= start and x < pivot_position else colors.YELLOW if x == pivot_position
                        else colors.DARK_BLUE if x > pivot_position and x <=end else colors.BLUE for x in range(len(data))])
        time.sleep(timeTick)
        
    drawData(data, [colors.BLUE for x in range(len(data))])