# -------------------------------------------------------------------------------------------------------------------
# Algorithm:    Bubble Sort
#
# Description:  Bubble Sort is the simplest sorting algorithm, 
#               that works by repeatedly swapping the adjacent elements if they are in the wrong order.
#               This algorithm is not suitable for large data sets as its average 
#               and worst case time complexity is quite high.
# -------------------------------------------------------------------------------------------------------------------

import time
import colors

def bubble_sort(data, drawData, timeTick):
    size = len(data)
    for i in range(size-1):
        for j in range(size-i-1):
            if data[j] > data[j+1]:
                data[j], data[j+1] = data[j+1], data[j]
                drawData(data, [colors.YELLOW if x == j or x == j+1 else colors.BLUE for x in range(len(data))] )
                time.sleep(timeTick)
                
    drawData(data, [colors.BLUE for x in range(len(data))])