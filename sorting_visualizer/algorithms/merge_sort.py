# -------------------------------------------------------------------------------------------------------------------
# Algorithm:    Quick Sort
#
# Description:  Like QuickSort, Merge Sort is a Divide and Conquer algorithm.
#               It divides the input array into two halves, calls itself for the two halves, 
#               and then it merges the two sorted halves.
#
#               The merge() function is used for merging two halves.
#               The merge(data, start, mid, end) is a key process that assumes that data[start..mid] and data[mid+1..end] are sorted 
#               and merges the two sorted sub-arrays into one.
# -------------------------------------------------------------------------------------------------------------------

import time
import colors

def merge(data, start, mid, end):
    p = start
    q = mid + 1
    tempArray = []

    for i in range(start, end+1):
        if p > mid:
            tempArray.append(data[q])
            q+=1
        elif q > end:
            tempArray.append(data[p])
            p+=1
        elif data[p] < data[q]:
            tempArray.append(data[p])
            p+=1
        else:
            tempArray.append(data[q])
            q+=1

    for p in range(len(tempArray)):
        data[start] = tempArray[p]
        start += 1

def merge_sort(data, start, end, drawData, timeTick):
    if start < end:
        mid = int((start + end) / 2)
        merge_sort(data, start, mid, drawData, timeTick)
        merge_sort(data, mid+1, end, drawData, timeTick)

        merge(data, start, mid, end)

        drawData(data, [colors.PURPLE if x >= start and x < mid else colors.YELLOW if x == mid 
                        else colors.DARK_BLUE if x > mid and x <=end else colors.BLUE for x in range(len(data))])
        time.sleep(timeTick)

    drawData(data, [colors.BLUE for x in range(len(data))])
    