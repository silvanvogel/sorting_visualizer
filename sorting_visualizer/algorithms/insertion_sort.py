# -------------------------------------------------------------------------------------------------------------------
# Algorithm:    Insertion Sort
#
# Description:  Insertion sort is a simple sorting algorithm 
#               that works similar to the way you sort playing cards in your hands. 
#               The array is virtually split into a sorted and an unsorted part. 
#               Values from the unsorted part are picked and placed at the correct position in the sorted part.
#
#               Characteristics of Insertion Sort:
#                - This algorithm is one of the simplest algorithm with simple implementation
#                - Basically, Insertion sort is efficient for small data values
#                - Insertion sort is adaptive in nature, i.e. 
#                  it is appropriate for data sets which are already partially sorted.
# -------------------------------------------------------------------------------------------------------------------

import time
import colors

def insertion_sort(data, drawData, timeTick):
    for i in range(len(data)):
        temp = data[i]
        k = i
        while k > 0 and temp < data[k-1]:
            data[k] = data[k-1]
            k -= 1
        data[k] = temp
        drawData(data, [colors.YELLOW if x == k or x == i else colors.BLUE for x in range(len(data))])
        time.sleep(timeTick)
        
    drawData(data, [colors.BLUE for x in range(len(data))])