# Import modules
import tkinter as tk
from tkinter import ttk
import random
import colors

# Import algorithms 
from algorithms.bubble_sort import bubble_sort
from algorithms.merge_sort import merge_sort
from algorithms.heap_sort import heap_sort
from algorithms.quick_sort import quick_sort
from algorithms.selection_sort import selection_sort
from algorithms.insertion_sort import insertion_sort


##############################################################
#  Main Window
##############################################################

# Initialize main window
window = tk.Tk()
window.title("Sorting Algorithms Visualization")
window.maxsize(1000, 700)
window.config(bg = colors.WHITE)

# Algorithm dropdown
algorithm_name = tk.StringVar()
algo_list = ["Bubble Sort", "Merge Sort", "Heap Sort", "Quick Sort", "Selection Sort", "Insertion Sort"]

# Speed dropdown
speed_name = tk.StringVar()
speed_list = ["Fast", "Medium", "Slow"]


##############################################################
#  Functions
##############################################################

data = []

# Draw the numerical array as vertical bars on the canvas
def drawData(data, colorArray):
    canvas.delete("all")
    canvas_width = 800
    canvas_height = 400
    x_width = canvas_width / (len(data) + 1)
    offset = 4
    spacing = 2
    normalizedData = [i / max(data) for i in data]

    for i, height in enumerate(normalizedData):
        x0 = i * x_width + offset + spacing
        y0 = canvas_height - height * 390
        x1 = (i + 1) * x_width + offset
        y1 = canvas_height
        canvas.create_rectangle(x0, y0, x1, y1, fill=colorArray[i])

    window.update_idletasks()

# Generate array with random values
def generate():
    global data

    data = []
    for i in range(0, 100):
        random_value = random.randint(1, 150)
        data.append(random_value)

    drawData(data, [colors.BLUE for x in range(len(data))])

# Set sorting speed
def set_speed():
    if speed_menu.get() == "Slow":
        return 0.3
    elif speed_menu.get() == "Medium":
        return 0.1
    else:
        return 0.001

# Execute the selected algorithm
def sort():
    global data
    timeTick = set_speed()
    
    if algo_menu.get() == "Bubble Sort":
        bubble_sort(data, drawData, timeTick)
        
    elif algo_menu.get() == "Merge Sort":
        merge_sort(data, 0, len(data)-1, drawData, timeTick)
        
    elif algo_menu.get() == "Heap Sort":
        heap_sort(data, drawData, timeTick)
        
    elif algo_menu.get() == "Quick Sort":
        quick_sort(data, 0, len(data)-1, drawData, timeTick)
        
    elif algo_menu.get() == "Selection Sort":
        selection_sort(data, drawData, timeTick)
        
    elif algo_menu.get() == "Insertion Sort":
        insertion_sort(data, drawData, timeTick)


##############################################################
#  User Interface
##############################################################

UI_frame = tk.Frame(window, width= 900, height=300, bg=colors.WHITE)
UI_frame.grid(row=0, column=0, padx=10, pady=5)

# label for algorithm
label_algorithm = tk.Label(UI_frame, text="Algorithm: ", bg=colors.WHITE)
label_algorithm.grid(row=0, column=0, padx=10, pady=5, sticky=tk.W)

# dropdown for algorithm
algo_menu = ttk.Combobox(UI_frame, textvariable=algorithm_name, values=algo_list)
algo_menu.grid(row=0, column=1, padx=5, pady=5)
algo_menu.current(0)

# label for sorting speed 
label_speed = tk.Label(UI_frame, text="Sorting Speed: ", bg=colors.WHITE)
label_speed.grid(row=1, column=0, padx=10, pady=5, sticky=tk.W)

# dropdown for sorting speed
speed_menu = ttk.Combobox(UI_frame, textvariable=speed_name, values=speed_list)
speed_menu.grid(row=1, column=1, padx=5, pady=5)
speed_menu.current(0)

# sort button 
button_sort = tk.Button(UI_frame, text="Sort", command=sort, bg=colors.LIGHT_GRAY)
button_sort.grid(row=2, column=1, padx=5, pady=5)

# button to generate array
button_generate = tk.Button(UI_frame, text="Generate Data", command=generate, bg=colors.LIGHT_GRAY)
button_generate.grid(row=2, column=0, padx=5, pady=5)

# canvas to draw our array 
canvas = tk.Canvas(window, width=800, height=400, bg=colors.WHITE)
canvas.grid(row=1, column=0, padx=10, pady=5)

window.mainloop()